import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

const API_ROOT = 'http://gbcs-dev.embl.de:81/api/'


// Dumb/Presentational component to show details of a sample
// Presentational means that this component is not aware of state, it just has access to props
export default class SampleDetail extends Component {
  render() {
    const { sample } = this.props
    const api_full_url = API_ROOT + 'samples/' + sample.id
    return (
      <div className="SampleDetail">
        <a href={sample.url}>
          <h3>{sample.name}</h3>
          <span>{sample.description}</span>
          <table><tr><td>{sample.id}</td></tr></table>
        </a>
      </div>
    )
  }
}

SampleDetail.propTypes = {
  sample: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string
  }).isRequired
}
