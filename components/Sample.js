import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'


// Dumb component for showing the given property in html
export default class Sample extends Component {
  render() {
    const { id, name, description } = this.props.sample

    return (
      <div className="Sample">
        <Link to={`/samples/${id}`}>
          <h3>{name}</h3>
          <span>{description}</span>
        </Link>
      </div>
    )
  }
}

Sample.propTypes = {
  sample: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string
  }).isRequired
}
