import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { loadSamples } from '../actions'
import List from '../components/List'
import zip from 'lodash/array/zip'
import SampleDetail from '../components/SampleDetail'
import { Link } from 'react-router'


function loadData(props) {
  const { sample_id } = props
  // props.loadStarred(login)
  props.loadSamples(sample_id)
}

class SampleDetailPage extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    loadData(this.props)
  }

  render() {
    const { sample } = this.props
    if (!sample) {
      return <h1><i>Loading Sample...</i></h1>
    }

    return (
      <div>
        <Link to={'/samples/'}><span>Go back</span></Link>
        <SampleDetail sample={sample}/>
      </div>
    )
  }
}

SampleDetailPage.propTypes = {
  sample: PropTypes.object,
}

function mapStateToProps(state) {
  const { sample_id } = state.router.params
  const {
    entities: { samples }
  } = state
  return {
    sample: samples[sample_id]
  }
}

export default connect(mapStateToProps, {
  // loadUser,
  // loadStarred,
  loadSamples,
})(SampleDetailPage)
