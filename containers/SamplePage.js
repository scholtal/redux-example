import React, { Component, PropTypes } from 'react'
//import DataGrid from 'react-datagrid'
import ReactDataGrid from 'react-data-grid/addons';
import { connect } from 'react-redux'
import { loadSamples } from '../actions'
import List from '../components/List'
import zip from 'lodash/array/zip'
import Sample from '../components/Sample'


/*
  This is a container or smart component that is aware of state and calls/bind action creators
*/

function loadData(props) {
  props.loadSamples(null)
  // const { login } = props
  // props.loadStarred(login)
}

class SamplePage extends Component {
  constructor(props) {
    super(props)
    this.handleLoadMoreClick = this.handleLoadMoreClick.bind(this)
    this.renderSample = this.renderSample.bind(this)
  }

  componentWillMount() {
    console.log("MOUNTED")
    //this.props.loadSamples(this.props.login)
    // this.props.loadSamples(null)
    loadData(this.props)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.fullName !== this.props.fullName) {
      loadData(nextProps)
    }
  }
  handleLoadMoreClick() {
    // this.props.loadStarred(this.props.login, true)
  }

  renderSample(sample) {
    console.log(sample)
    return (
      <Sample sample={sample} />
    )
  }

  render() {
    const { samples, sample_list } = this.props
    if (!samples) {
      return <h1><i>Loading Samples...</i></h1>
    }
    console.log("RENDER")
    let columns = [
      { key: 'id', name: '#'},
      { key: 'name', name: 'Name', editable: true},
      { key: 'description', name: 'Description', editable: true }
    ]

    /* react-data-grid */
    return <ReactDataGrid
          columns={columns}
          rowGetter={
              function(rowIndex) {
                return sample_list[rowIndex];
              }
          }
          rowsCount={sample_list.length}
          minHeight={500}
          enableCellSelect={true}/>


    /* react-datagrid:
    return <div><DataGrid
      dataSource={sample_list}
      columns={columns}
      idProperty='id'
    /></div>
    */
    /*
    let sample_list = []
    Object.keys(samples).forEach((key) =>
      sample_list.push(this.renderSample(samples[key]))
    )
    return (
      <div>
        <h1>Samples</h1>
        <hr />
        {sample_list}
      </div>
    )*/
  }
}

SamplePage.propTypes = {
  samples: PropTypes.object,
  sample_list: PropTypes.array
}

function mapStateToProps(state) {
  const {
    entities: { samples }
  } = state
  let sample_list = []
  Object.keys(samples).forEach((key) =>
    sample_list.push({
      'id': samples[key]['id'],
      'name': samples[key]['name'].substring(1,10),
      'description': samples[key]['description'],
    })
  )
  return {
    samples, // state.entities.samples
    sample_list
  }
}

export default connect(mapStateToProps, {
  // loadUser,
  // loadStarred,
  loadSamples,
})(SamplePage)
