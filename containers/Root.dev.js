import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import { ReduxRouter } from 'redux-router'
import DevTools from './DevTools'

export default class Root extends Component {
  render() {
    const { store } = this.props
    /*
    To hook up the store, we need to create the store somewhere at the root of our component hierarchy.
    For client apps, the root component is a good place. For server rendering, you can do this in the request handler.
    The trick is to wrap the whole view hierarchy into a <Provider> from React Redux.
    */
    return (
      <Provider store={store}>
        <div>
          {/* ReduxRouter - not sure why this is needed here..as the routes are configured in the store,
              but I guess this ensures the route details to be accessible in the state */}
          <ReduxRouter />
          <DevTools />
        </div>
      </Provider>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired
}
