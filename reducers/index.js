import * as ActionTypes from '../actions'
import merge from 'lodash/object/merge'
import paginate from './paginate'
import { routerStateReducer as router } from 'redux-router'
import { combineReducers } from 'redux'

// Updates an entity cache in response to any action with response.entities.
function entities(state = { users: {}, repos: {}, samples: {} }, action) {
  // the above notation gives state a initialstate i.e. if no state (and action) is given
  if (action.response && action.response.entities) {
    // if there is an action with a response and response.entities
    // call this merge function that will create a new object ({}) with the state and action.response.entities merged
    return merge({}, state, action.response.entities)
  }
  return state
}

// Updates error message to notify about the failed fetches.
function errorMessage(state = null, action) {
  const { type, error } = action

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null
  } else if (error) {
    return action.error
  }

  return state
}

// Updates the pagination data for different actions.
/*const pagination = combineReducers({
  samplesListPaginate: paginate({
    mapActionToKey: action => action.id,
    types: [
      ActionTypes.SAMPLE_REQUEST,
      ActionTypes.SAMPLE_SUCCESS,
      ActionTypes.SAMPLE_FAILURE
    ]
  }),
})*/

const rootReducer = combineReducers({
  entities,
  //pagination,
  errorMessage,
  router
})

export default rootReducer
