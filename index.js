require("babel-polyfill")
import 'bootstrap/dist/css/bootstrap.css'
//require('react-datagrid/index.css')
import React from 'react'
import { render } from 'react-dom'
import Root from './containers/Root'
import configureStore from './store/configureStore'
import './react-data-grid.css';


const store = configureStore()

// this renders the smart view component Root which in turn binds the store
render(
  <Root store={store} />,
  document.getElementById('root')
)
