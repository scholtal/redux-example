import React from 'react'
import { Route, NoMatch } from 'react-router'
import App from './containers/App'
import SamplePage from './containers/SamplePage'
import SampleDetailPage from './containers/SampleDetailPage'


export default (
  <Route path="/" component={App}>
    <Route path="/samples/:sample_id"
      component={SampleDetailPage} />
    <Route path="/samples"
        component={SamplePage} />
    <Route path="/*" component={NoMatch}/>
  </Route>
)
