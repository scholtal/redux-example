import { CALL_API, Schemas } from '../middleware/api'

export const USER_REQUEST = 'USER_REQUEST'
export const USER_SUCCESS = 'USER_SUCCESS'
export const USER_FAILURE = 'USER_FAILURE'

// Fetches a single user from Github API.
// Relies on the custom API middleware defined in ../middleware/api.js.
function fetchUser(login) {
  return {
    [CALL_API]: {
      types: [ USER_REQUEST, USER_SUCCESS, USER_FAILURE ],
      endpoint: `users/${login}`,
      schema: Schemas.USER
    }
  }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadUser(login, requiredFields = []) {
  return (dispatch, getState) => {
    const user = getState().entities.users[login]
    if (user && requiredFields.every(key => user.hasOwnProperty(key))) {
      return null
    }

    return dispatch(fetchUser(login))
  }
}


export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'

// Resets the currently visible error message.
export function resetErrorMessage() {
  return {
    type: RESET_ERROR_MESSAGE
  }
}


/* embase-ng samples */

export const SAMPLE_REQUEST = 'SAMPLE_REQUEST'
export const SAMPLE_SUCCESS = 'SAMPLE_SUCCESS'
export const SAMPLE_FAILURE = 'SAMPLE_FAILURE'

// Fetches a single repository from Github API.
// Relies on the custom API middleware defined in ../middleware/api.js.
// This would be called the action creator as it defines the action format
function fetchSamples() {
  console.log("fetchSamples CALLED!")
  return {
    [CALL_API]: {
      types: [ SAMPLE_REQUEST, SAMPLE_SUCCESS, SAMPLE_FAILURE ],
      endpoint: `samples/`,
      schema: Schemas.SAMPLES_ARRAY
    }
  }
}

// Fetches a single repository from Github API unless it is cached.
// Relies on Redux Thunk middleware.
// loadSamples is still part of the view/smart/container as it asks for a formatted action,
// that it will then dispatch to the store.
export function loadSamples() {
  return (dispatch, getState) => {
    console.log("loadSamples CALLED!")
    const sample_list = getState().entities.samples
    //if (sample_list != {}) {
    //  return null
    //}
    console.log(sample_list)
    return dispatch(fetchSamples())
  }
}
